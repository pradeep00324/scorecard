const template = document.createElement('template');
template.innerHTML = `
  <style>

  body {
	margin: 0;
	font-family: 'Roboto', sans-serif;
	font-size: 16px;
}

h1 {
  text-align: center;
  margin-bottom: 1.5em;
}

h2 {
	text-align: center;
	color: #555;
  margin-bottom: 0;
}

* { box-sizing: border-box; }

body {
  font-family: sans-serif;
  text-align: center;
}

.scene {
  position: relative;
  width: 200px;
  height: 200px;
  perspective: 1000px;
}

.carousel {
  width: 100%;
  height: 100%;
  position: absolute;
  transform: translateZ(-288px);
  transform-style: preserve-3d;
  transition: transform 1s;
}

    .carousel__cell {
    position: absolute;
    z-index: 9;
    background-color: #FFF;
    text-align: center;
    width: 200px;
    height:200px;
    box-shadow: 0 0 0 1px rgba(#000, .05), 0 20px 50px 0 rgba(#000, .1);
    box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
    border-radius: 15px;
    padding: 1.25rem;
    bottom:20px;
    right: 20px;
    transition: transform 1s;
    }
    #cardRoot {
        position: absolute;
        z-index: 9;
        background-color: #FFF;
        text-align: center;
        width: 200px;
        height:fit-content;
        box-shadow: 0 0 0 1px rgba(#000, .05), 0 20px 50px 0 rgba(#000, .1);
        box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
        border-radius: 15px;
        bottom:20px;
        right: 20px;
    }

.carousel__cell:nth-child(1) { transform: rotateY(  0deg) translateZ(288px); }
.carousel__cell:nth-child(2) { transform: rotateY( 40deg) translateZ(288px); }
.carousel__cell:nth-child(3) { transform: rotateY( 80deg) translateZ(288px); }
	
}
  </style>
  <div class="scene" id="cardRoot">
  <div class="carousel">
    <div class="carousel__cell">
    <div class="cardOne match-score" id="match-score" >
    <h6>Match Score</h6>
    <slot name="match-score"/>
    </div>
    </div>
    <div class="carousel__cell">
    <div class="cardTwo" id="match-details">
          <h6>Match Details</h6>
          <slot name="match-details"/>
          </div>
    </div>
    <div class="carousel__cell"> <div class="cardThree" >
    <img width="150px" height="150px" />
</div></div>
</div>
</div>

`;

var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

class ScoreCard extends HTMLElement {
    constructor() {
        super();

        this.showInfo = true;

        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.shadowRoot.querySelector('img').src = this.getAttribute('randomImage');
        this.elmnt = this.shadowRoot.getElementById("cardRoot");
        this.dragElement(this.shadowRoot);
        this.myCarosal(this.shadowRoot)

    }

    dragElement = (document) => {
        try {
            if (document.getElementById(this.elmnt.id + "header")) {

                document.getElementById(this.elmnt.id + "header").onmousedown = this.dragMouseDown;
            } else {

                this.elmnt.onmousedown = this.dragMouseDown;
            }
        }
        catch (err) {
            console.log(err, "errrrr")
        }
    }

    dragMouseDown = (e) => {
        e = e || window.event;
        e.preventDefault();

        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = this.closeDragElement;

        document.onmousemove = this.elementDrag;
    }

    elementDrag = (e) => {
        e = e || window.event;
        e.preventDefault();

        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;

        this.elmnt.style.top = (this.elmnt.offsetTop - pos2) + "px";
        this.elmnt.style.left = (this.elmnt.offsetLeft - pos1) + "px";
    }

    closeDragElement = () => {

        document.onmouseup = null;
        document.onmousemove = null;
    }

    toggleInfo() {
        this.showInfo = !this.showInfo;

        const info = this.shadowRoot.querySelector('.info');
        const toggleBtn = this.shadowRoot.querySelector('#toggle-info');

        if (this.showInfo) {
            info.style.display = 'block';
            toggleBtn.innerText = 'Hide Info';
        } else {
            info.style.display = 'none';
            toggleBtn.innerText = 'Show Info';
        }
    }

    myCarosal(document) {
        var carousel = document.querySelector('.carousel');
        var cells = carousel.querySelectorAll('.carousel__cell');
        var cellCount;
        var selectedIndex = 0;
        var cellWidth = carousel.offsetWidth;
        var cellHeight = carousel.offsetHeight;
        var isHorizontal = true;
        var rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
        var radius, theta;
        // console.log( cellWidth, cellHeight );

        function rotateCarousel() {
            var angle = theta * selectedIndex * -1;
            carousel.style.transform = 'translateZ(' + -radius + 'px) ' +
                rotateFn + '(' + angle + 'deg)';
        }

        function changeCarousel() {
            cellCount = 3;
            theta = 360 / cellCount;
            var cellSize = isHorizontal ? cellWidth : cellHeight;
            radius = Math.round((cellSize / 2) / Math.tan(Math.PI / cellCount));
            for (var i = 0; i < cells.length; i++) {
                var cell = cells[i];
                if (i < cellCount) {
                    cell.style.opacity = 1;
                    var cellAngle = theta * i;
                    cell.style.transform = rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
                } else {
                    cell.style.opacity = 0;
                    cell.style.transform = 'none';
                }
            }

            setInterval(() => {
                selectedIndex++;
                rotateCarousel();
            }, 2000)
        }
        changeCarousel();

    }
}

window.customElements.define('score-card', ScoreCard);